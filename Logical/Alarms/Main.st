
PROGRAM _INIT
	MpAlarmXCore_0.MpLink := ADR(gAlarmXCore);
	MpAlarmXCore_0.Enable := TRUE;
	MpAlarmXListUI_0.MpLink := ADR(gAlarmXCore);
	MpAlarmXListUI_0.Enable := TRUE;
	MpAlarmXListUI_0.UIConnect := ADR(MpAlarmXListUIConnect);
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	// backtrace for motion alarms
	
	IF MpAlarmXCore_0.PendingAlarms = 0 THEN
		// No pending alarms, reset instance id
		InstanceID := 0;
	END_IF
	
	maxIndex := SIZEOF(MpAlarmXListUIConnect.AlarmList.InstanceID) / SIZEOF(MpAlarmXListUIConnect.AlarmList.InstanceID[0]) - 1;
	FOR i := 0 TO maxIndex DO
		IF MpAlarmXListUIConnect.AlarmList.InstanceID[i] = InstanceID THEN
			MpAlarmXListUIConnect.AlarmList.SelectedIndex := i;
			EXIT;
		END_IF
	END_FOR
	 
	IF userAlarm THEN
		userAlarm := FALSE;
		MpAlarmXSet(gAlarmXCore, 'userAlarm');
	END_IF
	
	MpAlarmXCore_0();
	MpAlarmXListUI_0();
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

